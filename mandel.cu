extern "C" __global__ void render(int *out, int width, int height) {
  unsigned int x_dim = blockIdx.x*blockDim.x + threadIdx.x;
  unsigned int y_dim = blockIdx.y*blockDim.y + threadIdx.y;
  int index = width*y_dim + x_dim;
  float x_origin = ((float) x_dim/width)*3.25 - 2;
  float y_origin = ((float) y_dim/width)*2.5 - 1.25;

  float x = 0.0;
  float y = 0.0;

  int iteration = 0;
  int max_iteration = 256;
  while(x*x + y*y <= 4 && iteration < max_iteration) {
    float xtemp = x*x - y*y + x_origin;
    y = 2*x*y + y_origin;
    x = xtemp;
    iteration++;
  }

  if(iteration == max_iteration) {
    out[index] = 0;
  } else {
    out[index] = iteration;
  }
}
