require 'ruby_parser'
require 'sexp_processor'
require './type'
require './typed_sexp'
require './variables'

##
# TypeChecker inferences types for sexps using type unification.
#
# TypeChecker expects sexps rewritten with Rewriter, and outputs TypedSexps.
#
# Nodes marked as 'unsupported' do not do correct type-checking of all the
# pieces of the node.  They generate possibly incorrect output, that is all.

class TypeInferencer < SexpProcessor

  ##
  # Environment containing local variables

  attr_reader :env

  ##
  # The global environment contains global variables and constants.

  attr_reader :genv

  ##
  # Function table

  attr_reader :functions

  def initialize
    super
    @env = ::Variables.new
    @genv = ::Variables.new
    #@functions = FunctionTable.new
    self.auto_shift_type = true
    self.expected = TypedSexp

    self.unsupported = [:alias, :alloca, :argscat, :argspush, :attrset,
                        :back_ref, :bmethod, :break, :case, :cdecl, :cfunc,
                        :cref, :cvdecl, :dasgn, :defs, :dmethod, :dot2, :dot3,
                        :dregx, :dregx_once, :dsym, :dxstr, :evstr, :fbody,
                        :fcall, :flip2, :flip3, :for, :ifunc, :last, :match,
                        :match2, :match3, :memo, :method, :module, :newline,
                        :next, :nth_ref, :op_asgn1, :op_asgn2, :op_asgn_and,
                        :opt_n, :postexe, :redo, :retry, :sclass, :svalue,
                        :undef, :until, :valias, :vcall, :when, :xstr, :zarray,
                        :zsuper]
  end

  def check_builtin_variables(exp)
    # blockIdx
    if    exp == s(s(:call, nil, :blockIdx), :x)
      return t(:call, t(:call, nil, :blockIdx, Type.unknown), :x, Type.int)
    elsif exp == s(s(:call, nil, :blockIdx), :y)
      return t(:call, t(:call, nil, :blockIdx, Type.unknown), :y, Type.int)
    elsif exp == s(s(:call, nil, :blockIdx), :z)
      return t(:call, t(:call, nil, :blockIdx, Type.unknown), :z, Type.int)
    # blockDim
    elsif exp == s(s(:call, nil, :blockDim), :x)
      return t(:call, t(:call, nil, :blockDim, Type.unknown), :x, Type.int)
    elsif exp == s(s(:call, nil, :blockDim), :y)
      return t(:call, t(:call, nil, :blockDim, Type.unknown), :y, Type.int)
    elsif exp == s(s(:call, nil, :blockDim), :z)
      return t(:call, t(:call, nil, :blockDim, Type.unknown), :z, Type.int)
    # threadIdx
    elsif exp == s(s(:call, nil, :threadIdx), :x)
      return t(:call, t(:call, nil, :threadIdx, Type.unknown), :x, Type.int)
    elsif exp == s(s(:call, nil, :threadIdx), :y)
      return t(:call, t(:call, nil, :threadIdx, Type.unknown), :y, Type.int)
    elsif exp == s(s(:call, nil, :threadIdx), :z)
      return t(:call, t(:call, nil, :threadIdx, Type.unknown), :z, Type.int)
    # gridDim
    elsif exp == s(s(:call, nil, :gridDim), :x)
      return t(:call, t(:call, nil, :gridDim, Type.unknown), :x, Type.int)
    elsif exp == s(s(:call, nil, :gridDim), :y)
      return t(:call, t(:call, nil, :gridDim, Type.unknown), :y, Type.int)
    elsif exp == s(s(:call, nil, :gridDim), :z)
      return t(:call, t(:call, nil, :gridDim, Type.unknown), :z, Type.int)

    else
      return nil
    end
  end

  def process_function(exp, arg_types)
    @arg_types = arg_types
    process exp
  end

  ##
  # Logical and unifies its two arguments, then returns a bool sexp.

  def process_and(exp)
    rhs = process exp.shift
    lhs = process exp.shift

    return t(:and, rhs, lhs, Type.bool)
  end

  ##
  # Args list adds each variable to the local variable table with unknown
  # types, then returns an untyped args list of name/type pairs.

  def process_args(exp)
    formals = t(:args)
    types = []

    until exp.empty? do
      arg = exp.shift
      type = @env.lookup(arg) rescue nil
      if type.nil?
        type = Type.new(@arg_types.shift)
        @env.add arg, type
      else
        @arg_types.shift
      end
      formals << t(arg, type)
      types << type
    end

    return formals
  end

  ##
  # Attrasgn processes its rhs and lhs, then returns an untyped sexp.
  #--
  # TODO rewrite this in Rewriter
  # echo "self.blah=7" | parse_tree_show -f
  # => [:attrasgn, [:self], :blah=, [:array, [:lit, 7]]]

  def process_attrasgn(exp)
    receiver = process exp.shift
    name = exp.shift
    rhs = process exp.pop

    case name
    when :[]=
      idx = process exp.shift
      if receiver.sexp_type.unknown?
        new_type = Type.new(rhs.sexp_type.type.contents, true)
        receiver.sexp_type = new_type
        @env[receiver[1]] = [new_type]
      end
      return t(:attrasgn, receiver, name, idx, rhs)
    else
    end

    return t(:attrasgn, receiver, name, rhs)
  end

  ##
  # Call unifies the actual function paramaters against the formal function
  # paramaters, if a function type signature already exists in the function
  # table.  If no type signature for the function name exists, the function is
  # added to the function list.
  #
  # Returns a sexp returned to the type of the function return value, or
  # unknown if it has not yet been determined.

  def process_call(exp)
    builtin = check_builtin_variables(exp)
    if builtin
      exp.clear
      return builtin
    end

    receiver = exp.shift
    name = exp.shift
    args = []

    until exp.empty? do
      arg = exp.shift
      next if arg.empty?
      args << arg
    end

    receiver = process receiver
    return_type = Type.unknown

    case name
    when *[:+, :-, :*, :/, :%]
      binarg = process args[0]
      args = binarg
      if receiver.sexp_type == Type.unknown || binarg.sexp_type == Type.unknown
        return_type = receiver.sexp_type.unknown? ? binarg.sexp_type : receiver.sexp_type
      elsif receiver.sexp_type == binarg.sexp_type
        return_type = receiver.sexp_type
      else
        return_type = Type.float
      end
    when *[:<, :<=, :>, :>=, :==, :"!="]
      binarg = process args[0]
      args = binarg
      if receiver.sexp_type.is_float? || binarg.sexp_type.is_double?
        return_type = Type.float
      elsif
        return_type = Type.int
      end
    when :[]
      idx = process args[0]
      args = idx
      return_type = Type.send receiver.sexp_type.type.contents
    when :to_f
      return_type = Type.float
    when :to_i
      return_type = Type.int
    else
      if receiver.nil?
        return_type = @env.lookup(name)
      end
    end

    return t(:call, receiver, name, args, return_type)
  end

  ##
  # Defn adds the formal argument types to the local environment and attempts
  # to unify itself against the function table.  If no function exists in the
  # function table, defn adds itself.
  #
  # Defn returns a function-typed sexp.

  def process_defn(exp)
    name = exp.shift
    unprocess_args = exp.shift
    body = []
    base = t(:defn, name)

    @env.scope do
      args = process unprocess_args.dup
      base << args
      until exp.empty? do
        t = process exp.shift
        base << t
      end
      # reprocess args to set arg types
      args = process unprocess_args
      base[2] = args
    end

    base.sexp_type = Type.void
    return base
  end

  ##
  # Instance variable assignment is currently unsupported.  Does no
  # unification and returns an untyped sexp

  def process_iasgn(exp)
    var = exp.shift
    val = process exp.shift

    var_type = @env.lookup var rescue nil
    if var_type.nil? then
      @env.add var, val.sexp_type
    else
      val.sexp_type.unify var_type
    end

    return t(:iasgn, var, val, val.sexp_type)
  end

  ##
  # If unifies the condition against the bool type, then unifies the return
  # types of the then and else expressions against each other.  Returns a sexp
  # typed the same as the then and else expressions.

  def process_if(exp)
    cond_exp = process exp.shift
    then_exp = process exp.shift
    else_exp = process exp.shift rescue nil # might be empty

    return t(:if, cond_exp, then_exp, else_exp, Type.unknown)
  end

  ##
  # Instance variables are currently unsupported.  Returns an unknown-typed
  # sexp.
  #--
  # TODO support instance variables

  def process_ivar(exp)
    name = exp.shift

    var_type = @env.lookup name rescue nil
    if var_type.nil? then
      var_type = Type.unknown
      @env.add name, var_type
    end

    return t(:ivar, name, var_type)
  end

  ##
  # Local variable assignment unifies the variable type from the environment
  # with the assignment expression, and returns a sexp of that type.  If there
  # is no local variable in the environment, one is added with the type of the
  # assignment expression and a sexp of that type is returned.
  #
  # If an lasgn has no value (inside masgn) the returned sexp has an unknown
  # Type and a nil node is added as the value.

  def process_lasgn(exp)
    name = exp.shift
    arg_exp = nil
    arg_type = Type.unknown
    var_type = @env.lookup name rescue nil

    unless exp.empty? then
      sub_exp = exp.shift
      sub_exp_type = sub_exp.first
      arg_exp = process sub_exp

      # if we've got an array in there, unify everything in it.
      if sub_exp_type == :array then
        arg_type = arg_exp.sexp_types
        arg_type = arg_type.inject(Type.unknown) do |t1, t2|
          t1.unify t2
        end
        arg_type = arg_type.dup # singleton type
        arg_type.list = true
      else
        arg_type = arg_exp.sexp_type
      end
    end

    if var_type.nil? then
      @env.add name, arg_type
      var_type = arg_type
    else
      var_type.unify arg_type
    end

    return t(:lasgn, name, arg_exp, var_type)
  end

  ##
  # Literal values return a sexp typed to match the literal expression.

  def process_lit(exp)
    value = exp.shift
    type = nil

    case value
    when Fixnum then
      type = Type.int
    when Float then
      type = Type.float
    when Symbol then
      type = Type.symbol
    when Regexp then
      type = Type.regexp
    when Range then
      type = Type.range
    when Const then
      type = Type.const
    else
      raise "Bug! no: Unknown literal #{value}:#{value.class}"
    end

    return t(:lit, value, type)
  end

  ##
  # Local variables get looked up in the local environment and a sexp of that
  # type is returned.

  def process_lvar(exp)
    name = exp.shift
    t = @env.lookup name
    return t(:lvar, name, t)
  end

  def process_while(exp)
    pp exp
    condition = process exp.shift
    body = process exp.shift
    head_controlled = exp.shift
    return t(:while, condition, body, head_controlled, Type.void)
  end
end
