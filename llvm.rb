require 'ruby_parser'
require 'llvm/core'
require 'llvm/execution_engine'
require 'tempfile'
require 'rubycu'
require 'sexp_processor'
require 'pp'
require 'pry'
require './type_inferencer'
require './ruby2nvvm'
require './sexp2nvvm'
require 'benchmark'
require 'RMagick'

include SGC::CU
include Magick

def register_kernel(method_name)
  kernel_params = self.method(method_name).parameters.map(&:last)
  kernel_name = "#{method_name}_kernel_core"

  self.class.class_eval do
    alias_method kernel_name, method_name

    define_method method_name do |threads, *args|
      @trans ||= Ruby2NVVM.new
      arg_types = args.map do |a|
        case a
        when Array
          [a.class, a[0].nil? ? :unknown : a[0].class]
        else
          a.class
        end
      end

      unless @trans.functions.include?([method_name, arg_types.dup])
        method = Pry::Method(self.method(kernel_name))
        sexp = RubyParser.new.parse method.source

        function = @trans.process_function method_name, arg_types, sexp
      else
        function = @trans.functions[[method_name, arg_types.dup]]
      end

      @trans.launch_kernel(function, threads, *args)
    end
  end
end

def vecadd(a, w, h)
  x_dim = blockIdx.x * blockDim.x + threadIdx.x
  y_dim = blockIdx.y * blockDim.y + threadIdx.y
  index = w * y_dim  + x_dim
  x_origin = x_dim.to_f / w * 3.25 - 2
  y_origin = y_dim.to_f / w * 2.5  - 1.25
  x = 0.0
  y = 0.0
  iter = 0
  max_iter = 256
  while (x * x + y * y <= 4 && iter < max_iter) do
    xtemp = x * x - y * y + x_origin
    y = 2 * x * y + y_origin
    x = xtemp
    iter += 1
  end

  if iter == max_iter
    a[index] = 0
  else
    a[index] = iter
  end
end

def mandelbrot(a, w, h, startx, starty, dx, dy)
  xpix = blockIdx.x * blockDim.x + threadIdx.x
  ypix = blockIdx.y * blockDim.y + threadIdx.y

  x = startx + x*xpix
  y = starty + y*dy
  r = x
  s = y
  iter = 0
  maxIter = 256
  while r * r + s * s <=4 && iter < maxIter do
    nextr = r * r - s * s + x
    nexts = 2 * r * s + y
    r = nextr
    s = nexts
  end

  if iter == max_iter
    a[index] = 0
  else
    a[index] = iter
  end
end

def write_bmp(w, h, data)
  image = Image.new(w, h)
  (0...h).each do |y|
    (0...w).each do |x|
      d = data[y * w + x]
      draw = Draw.new.fill("##{d.to_s(16)}#{d.to_s(16)}#{d.to_s(16)}")
      draw.point(x, y).draw(image)
    end
  end
  image.write("out.bmp")
end

register_kernel :vecadd
centerX = -0.5
centerY = 0
zoom = 300
w = 1024
h = 768
startx = centerX - width.to_f / (zoom * 2)
endx = centerX + width.to_f / (zoom * 2)
starty = centerY - height.to_f / (zoom * 2)
endy = centerX + width.to_f / (zoom * 2)
dx = endx - startx
dy = endy - starty
dx_over_width = dx / width
dx_over_height = dy / height
pp "prepare data"
a = Array.new(w * h) { 0 }
pp "launch"
#Benchmark.bm do |x|
#  x.report { vecadd([w, h], a, w, h) }
#end
#write_bmp(n, n, a)

CUInit.init
@dev = CUDevice.get(0)
@ctx = CUContext.create(@dev)
mod = CUModule.new
mod.load("/home/tomoyuki/Dropbox/work/work/ruby2nvvm/mandel.ptx")
da = CUDevice.malloc(4 * w * h)
f = mod.function("render")
f.launch_kernel(w/16, h/16, 1, 16, 16, 1, 0, 0, [da, w, h])
ha = Buffer.new(:int, w * h)
CUMemory.memcpy_dtoh(ha, da, 4 * w * h)
a = ha.ptr.read_array_of_int(w * h)
da.free
write_bmp(w, h, a)

# p c
