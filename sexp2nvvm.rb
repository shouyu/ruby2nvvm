require 'sexp_processor'
require 'composite_sexp_processor'
require './type'
require './typed_sexp'
require './type_inferencer'
require './variables'

class Sexp2NVVM < SexpProcessor
  attr_reader :inarg
  attr_reader :outarg
  attr_reader :args
  attr_reader :mod

  def initialize
    super
    self.auto_shift_type = true
    self.strict = true
    self.expected = Object

    @current_func = nil
    @current_block = nil
    @current_arg_types = nil
    @localvar = {}
    @args = []
    @inarg = {}
    @outarg = {}
    @intrinsics = {}
    @functions = {}
    @env = Variables.new
    @mod = LLVM::Module.new('ruby2nvvm')
    define_intrinsics
  end

  def llvm_module
    @mod ||= LLVM::Module.new('ruby2nvvm')
  end

  def define_intrinsics
    # threadIdx
    @intrinsics[:tidx]     = @mod.functions.add('llvm.nvvm.read.ptx.sreg.tid.x', [], LLVM::Int32)
    @intrinsics[:tidy]     = @mod.functions.add('llvm.nvvm.read.ptx.sreg.tid.y', [], LLVM::Int32)
    @intrinsics[:tidz]     = @mod.functions.add('llvm.nvvm.read.ptx.sreg.tid.z', [], LLVM::Int32)
    # blockDim
    @intrinsics[:ntidx]    = @mod.functions.add('llvm.nvvm.read.ptx.sreg.ntid.x', [], LLVM::Int32)
    @intrinsics[:ntidy]    = @mod.functions.add('llvm.nvvm.read.ptx.sreg.ntid.y', [], LLVM::Int32)
    @intrinsics[:ntidz]    = @mod.functions.add('llvm.nvvm.read.ptx.sreg.ntid.z', [], LLVM::Int32)
    # blockIdx
    @intrinsics[:ctaidx]   = @mod.functions.add('llvm.nvvm.read.ptx.sreg.ctaid.x', [], LLVM::Int32)
    @intrinsics[:ctaidy]   = @mod.functions.add('llvm.nvvm.read.ptx.sreg.ctaid.y', [], LLVM::Int32)
    @intrinsics[:ctaidz]   = @mod.functions.add('llvm.nvvm.read.ptx.sreg.ctaid.z', [], LLVM::Int32)
    # gridDim
    @intrinsics[:nctaidx]  = @mod.functions.add('llvm.nvvm.read.ptx.sreg.nctaid.x', [], LLVM::Int32)
    @intrinsics[:nctaidy]  = @mod.functions.add('llvm.nvvm.read.ptx.sreg.nctaid.y', [], LLVM::Int32)
    @intrinsics[:nctaidz]  = @mod.functions.add('llvm.nvvm.read.ptx.sreg.nctaid.z', [], LLVM::Int32)

    @intrinsics[:warpsize] = @mod.functions.add('llvm.nvvm.read.ptx.sreg.warpsize', [], LLVM::Int32)
  end

  def type_conversion(lhs, lhs_type, rhs, rhs_type, ret_type)
    if lhs_type.is_int? && ret_type.is_float?
      lhs = @current_block.si2fp lhs, LLVM::Float
    end
    if rhs_type.is_int? && ret_type.is_float?
      rhs = @current_block.si2fp rhs, LLVM::Float
    end
    return lhs, rhs
  end

  def add_kernel_metadata(func)
    vals = [func.to_ptr, LLVM::C.md_string("kernel", 6), LLVM::C.const_int(LLVM::Int32, 1, 0)]
    ptr = FFI::MemoryPointer.new(FFI.type_size(:pointer)*3)
    ptr.write_array_of_pointer(vals)
    mdnode = LLVM::C.md_node(ptr, 3)
    LLVM::C.add_named_metadata_operand(@mod, "nvvm.annotations", mdnode)
  end

  def check_builtin_variables(exp)
    # blockIdx
    if    exp == t(t(:call, nil, :blockIdx, Type.unknown), :x, Type.int)
      return @current_block.call @intrinsics[:"ctaidx"]
    elsif exp == t(t(:call, nil, :blockIdx, Type.unknown), :y, Type.int)
      return @current_block.call @intrinsics[:"ctaidy"]
    elsif exp == t(t(:call, nil, :blockIdx, Type.unknown), :z, Type.int)
      return @current_block.call @intrinsics[:"ctaidz"]
    # blockDim
    elsif exp == t(t(:call, nil, :blockDim, Type.unknown), :x, Type.int)
      return @current_block.call @intrinsics[:"ntidx"]
    elsif exp == t(t(:call, nil, :blockDim, Type.unknown), :y, Type.int)
      return @current_block.call @intrinsics[:"ntidy"]
    elsif exp == t(t(:call, nil, :blockDim, Type.unknown), :z, Type.int)
      return @current_block.call @intrinsics[:"ntidz"]
    # threadIdx
    elsif exp == t(t(:call, nil, :threadIdx, Type.unknown), :x, Type.int)
      return @current_block.call @intrinsics[:"tidx"]
    elsif exp == t(t(:call, nil, :threadIdx, Type.unknown), :y, Type.int)
      return @current_block.call @intrinsics[:"tidy"]
    elsif exp == t(t(:call, nil, :threadIdx, Type.unknown), :z, Type.int)
      return @current_block.call @intrinsics[:"tidx"]
    # gridDim
    elsif exp == t(t(:call, nil, :gridDim, Type.unknown), :x, Type.int)
      return @current_block.call @intrinsics[:"nctaidx"]
    elsif exp == t(t(:call, nil, :gridDim, Type.unknown), :y, Type.int)
      return @current_block.call @intrinsics[:"nctaidy"]
    elsif exp == t(t(:call, nil, :gridDim, Type.unknown), :z, Type.int)
      return @current_block.call @intrinsics[:"nctaidz"]

    else
      return nil
    end
  end

  def process_defn(exp)
    type1 = exp[1].first
    type2 = exp[2].first rescue nil
    expect = [:ivar, :iasgn, :attrset]

    comm = exp.comments
    name = exp.shift
    args = process exp.shift
    args = nil if args == "()"

    exp.shift if exp == s(s(:nil))

    @current_func = @mod.functions.add(name, args.map {|a| a[0]}, LLVM.Void)
    add_kernel_metadata @current_func
    @current_func.basic_blocks.append.build do |builder|
      @current_block = builder
      args.each_with_index {|a, i|
        addr = @current_block.alloca a[0], "#{a[1]}.addr"
        builder.store @current_func.params[i], addr
        @localvar[a[1]] = addr
        @args << a[1]
      }
      until exp.empty? do
        process exp.shift
      end
      @current_block.ret_void
    end

    return [@args, @inarg, @outarg]
  end

  def process_nil(exp)
    return
  end

  def process_args(exp)
    args = []

    until exp.empty? do
      arg = exp.shift
      arg_name = arg.shift
      type_name = arg.sexp_type
      if type_name.is_int?
        args << [LLVM::Int32, arg_name]
      elsif type_name.is_int_list?
        args << [LLVM::Type.pointer(LLVM::Int32, 1), arg_name]
        @inarg[arg_name] = :int
      elsif type_name.is_float_list?
        args << [LLVM::Type.pointer(LLVM::Float, 1), arg_name]
        @inarg[arg_name] = :float
      end
    end

    return args
  end

  # lasgn
  # ローカル変数への代入
  def process_lasgn(exp)
    varname = exp.shift
    rhs = process exp.shift

    if @localvar.include? varname
      lhs = @localvar[varname.to_sym]
    else
      # new variable
      if exp.sexp_type == Type.int
        lhs = @current_block.alloca LLVM::Int32, "#{varname}.addr"
      elsif exp.sexp_type == Type.float
        lhs = @current_block.alloca LLVM::Float, "#{varname}.addr"
      end
      @localvar[varname.to_sym] = lhs
    end

    @current_block.store rhs, lhs
  end

  # attrasgn
  # 属性への代入
  def process_attrasgn exp
    if exp[0][0] == :lvar
      lhs_varname = exp[0][1]
      lhs_typename = exp[0].sexp_type
    end
    receiver = process exp.shift
    name = exp.shift
    rhs = process exp.pop

    case name
    when :[]=
      idx = process exp.shift
      @outarg[lhs_varname] = lhs_typename.type.contents if lhs_varname
      lhs = @current_block.gep receiver, idx
      return @current_block.store rhs, lhs
    else
      raise "Unsupported"
    end
  end

  def process_call(exp)
    ret = check_builtin_variables(exp)
    if ret
      exp.clear
      return ret
    end

    ret_type = exp.sexp_type
    receiver = exp.shift
    receiver_type = receiver.sexp_type
    args = []

    until exp.empty? do
      arg = exp.shift
      next if arg.empty?
      args << arg
    end

    lhs = process receiver
    case args[0]
    when *[:+, :-, :*, :/, :%]
      arg_type = args[1].sexp_type
      rhs = process args[1]
      lhs, rhs = type_conversion(lhs, receiver_type, rhs, arg_type, ret_type)
      case args[0]
      when :+
        if ret_type.is_int?
          return @current_block.add lhs, rhs
        elsif ret_type.is_float?
          return @current_block.fadd lhs, rhs
        end
      when :-
        if ret_type.is_int?
          return @current_block.sub lhs, rhs
        elsif ret_type.is_float?
          return @current_block.fsub lhs, rhs
        end
      when :*
        if ret_type.is_int?
          return @current_block.mul lhs, rhs
        elsif ret_type.is_float?
          return @current_block.fmul lhs, rhs
        end
      when :/
        if ret_type.is_int?
          return @current_block.div lhs, rhs
        elsif ret_type.is_float?
          return @current_block.fdiv lhs, rhs
        end
      when :%
        if ret_type.is_int?
          return @current_block.rem lhs, rhs
        elsif ret_type.is_float?
          return @current_block.frem lhs, rhs
        end
      end
    when *[:<, :<=, :>, :>=, :==, :"!="]
      if exp.sexp_type == Type.int
        predicate =
          case args[0]
          when :<;     :slt;
          when :<=;    :sle;
          when :>;     :sgt;
          when :>=;    :sge;
          when :==;    :eq;
          when :"!=="; :ne;
          end
      elsif exp.sexp_type == Type.float
        predicate =
          case args[0]
          when :<;     :olt;
          when :<=;    :ole;
          when :>;     :ogt;
          when :>=;    :oge;
          when :==;    :oeq;
          when :"!=="; :one;
          end
      end
      arg_type = args[1].sexp_type
      rhs = process args[1]
      # Implicit Type Conversion
      lhs, rhs = type_conversion(lhs, receiver_type, rhs, arg_type, ret_type)
      if ret_type.is_int?
        return @current_block.icmp predicate, lhs, rhs
      elsif ret_type.is_float?
        return @current_block.fcmp predicate, lhs, rhs
      end
    when :[]
      idx = process args[1]
      lhs = @current_block.gep lhs, idx
      return @current_block.load lhs
    when :to_f
      return @current_block.si2fp lhs, LLVM::Float
    when :to_i
      return @current_block.fp2si lhs, LLVM::Int32
    else
      raise "Unsupported #{receiver.inspect}, #{args.inspect}"
    end
  end

  def process_lit(exp)
    obj = exp.shift
    if exp.sexp_type == Type.float
      return LLVM.Float(obj.to_f)
    elsif exp.sexp_type == Type.int
      return LLVM.Int(obj.to_i)
    end
  end

  def process_lvar(exp)
    varname = exp.shift
    if @localvar.include? varname
      return @current_block.load @localvar[varname]
    else
      raise
    end
  end

  def process_if(exp)
    condition = exp.shift
    condition = process condition
    trueblock = @current_func.basic_blocks.append
    falseblock = @current_func.basic_blocks.append
    exitblock = @current_func.basic_blocks.append
    @current_block.cond condition, trueblock, falseblock
    trueblock.build(@current_block) do |builder|
      t = process exp.shift
      @current_block.br exitblock
    end
    falseblock.build(@current_block) do |builder|
      f = process exp.shift
      @current_block.br exitblock
    end
    @current_block.position_at_end exitblock
    return nil
  end

  def process_and(exp)
    res = @current_block.alloca LLVM::Int32
    @current_block.store LLVM::Constant.null(LLVM::Int32), res
    lhs = process exp.shift
    rhsblock = @current_func.basic_blocks.append
    sucblock = @current_func.basic_blocks.append
    exitblock = @current_func.basic_blocks.append
    @current_block.cond lhs, rhsblock, exitblock
    rhsblock.build(@current_block) do |builder|
      rhs = process exp.shift
      @current_block.cond rhs, sucblock, exitblock
    end
    sucblock.build(@current_block) do |builder|
      @current_block.store LLVM.Int(1), res
      @current_block.br exitblock
    end
    @current_block.position_at_end exitblock

    return @current_block.load res
  end

  def process_while(exp)
    condition = exp.shift
    body = exp.shift
    head_controlled = exp.shift
    condblock = @current_func.basic_blocks.append
    exitblock = @current_func.basic_blocks.append
    bodyblock = @current_func.basic_blocks.append
    if head_controlled
      @current_block.br condblock
    end
    bodyblock.build(@current_block) do |builder|
      process body
      @current_block.br condblock
    end
    condblock.build(@current_block) do |builder|
      condition = process condition
      builder.cond condition, bodyblock, exitblock
    end
    @current_block.position_at_end exitblock
    return nil
  end

  def process_block(exp)
    process exp.shift until exp.empty?
  end
end

