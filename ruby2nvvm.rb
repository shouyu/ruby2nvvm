require 'rubycu'
require './type_inferencer'
require './sexp2nvvm'

class Ruby2NVVM < SexpProcessor
  include SGC::CU
  attr_accessor :functions

  def initialize
    super
    @current_arg_types = nil
    @type_inferencer = TypeInferencer.new
    @sexp2nvvm = Sexp2NVVM.new
    @functions = {}
    @machine = init_ptx_backend
    @ptx_code = nil
    CUInit.init
    @dev = CUDevice.get(0)
    @ctx = CUContext.create(@dev)
    @cu_module = CUModule.new
  end

  def llvm_module
    return @sexp2nvvm.mod
  end

  def ptx_code
    return @ptx_code
  end

  def generate_ptx
    llvm_module.dump
    tmp = Tempfile.open(['emit', '.ptx'])
    @machine.emit(llvm_module, tmp.path)
    puts tmp.read
    @cu_module.load(tmp.path)
  end

  def init_ptx_backend
    LLVM::Target.init_all(true)
    nvptx = LLVM::Target.by_name('nvptx64')
    return nvptx.create_machine('nvptx64-generic-generic', "sm_20")
  end

  def get_function(name, arg_types)
  end

  def process_function(name, arg_types, exp)
    @current_arg_types = arg_types.dup
    tsexp = @type_inferencer.process_function exp, arg_types.dup
    pp tsexp
    arg_names, inargs, outargs = @sexp2nvvm.process tsexp
    generate_ptx
    func_key = [name, arg_types].freeze
    functions[func_key] = {
      name:      name,
      arg_names: arg_names,
      inargs:    inargs,
      outargs:   outargs,
    }
    functions[func_key]
  end

  def launch_kernel(function, threads, *args)
    cuda_args = []
    t = Time.now
    pp 0
    function[:arg_names].each_with_index {|a, i|
      if function[:inargs].include? a
        if args[i][0] != nil
          typename = function[:inargs][a]
          ha = Buffer.new(typename, args[i].size)
          ha.ptr.send("write_array_of_#{typename}", args[i])
        end
        da = CUDevice.malloc(4 * args[i].size)
        CUMemory.memcpy_htod(da, ha, 4 * args[i].size) if ha
        cuda_args << da
      else
        cuda_args << args[i]
      end
    }
    pp (Time.now - t) * 1000.0

    f = @cu_module.function(function[:name].to_s)
    threads_per_block = 16
    if threads.class == Array
      tx = threads[0]
      ty = threads[1]
      bx = (tx + threads_per_block - 1) / threads_per_block
      by = (ty + threads_per_block - 1) / threads_per_block
      pp bx, by, threads_per_block, threads_per_block
      f.launch_kernel(bx, by, 1, threads_per_block, threads_per_block, 1, 0, 0, cuda_args)
    else
      blocks_per_grid = (threads + threads_per_block - 1) / threads_per_block
      f.launch_kernel(blocks_per_grid, 1, 1, threads_per_block, 1, 1, 0, 0, cuda_args)
    end
    pp (Time.now - t) * 1000.0

    function[:arg_names].each_with_index {|a, i|
      if function[:outargs].include? a
        typename = function[:inargs][a]
        ha = Buffer.new(typename, args[i].size)
        CUMemory.memcpy_dtoh(ha, cuda_args[i], args[i].size * 4)
        args[i][0,args[i].size] = ha.ptr.send("read_array_of_#{typename.to_s}", args[i].size)
      end
    }
    pp (Time.now - t) * 1000.0

    cuda_args.each {|a| a.free rescue nil }
  end
end
